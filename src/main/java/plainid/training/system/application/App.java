package plainid.training.system.application;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "apps")
public class App {

    @Id
    private String app_id;
    private String app_name;


    public App() {
    }

    public App(String app_id, String app_name) {
        this.app_id = app_id;
        this.app_name = app_name;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }
}
