package plainid.training.system.policy;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "policies")
public class Policy {


    @Id
    private String policy_id;
    private String policy_name;

    public Policy() {
    }

    public Policy(String policy_id, String policy_name) {
        this.policy_id = policy_id;
        this.policy_name = policy_name;
    }

    public String getPolicy_id() {
        return policy_id;
    }

    public void setPolicy_id(String policy_id) {
        this.policy_id = policy_id;
    }

    public String getPolicy_name() {
        return policy_name;
    }

    public void setPolicy_name(String policy_name) {
        this.policy_name = policy_name;
    }
}
