package plainid.training.system.user;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "users")
public class User {

    @Id
    private String user_id;
    private String user_name;
    private String department;
    private String country;
    @CreationTimestamp
    private Timestamp on_create;

    public User() {
    }

    public User(String user_id, String user_name, String department, String country, Timestamp on_create) {
        this.user_id = user_id;
        this.user_name = user_name;
        this.department = department;
        this.country = country;
        this.on_create = on_create;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Timestamp getOn_create() {
        return on_create;
    }

    public void setOn_create(Timestamp on_create) {
        this.on_create = on_create;
    }
}