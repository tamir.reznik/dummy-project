package plainid.training.system.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Create user and write it to db
     *
     * @return the user that saved to db
     */
    public User create(User user) {
        Optional<User> dbUser = this.userRepository.findById(user.getUser_id());
        return dbUser.orElseGet(() -> this.userRepository.save(user));
    }

    /**
     * Find user by id
     *
     * @return User with specific id
     */
    public User findById(String id) {
        Optional<User> optionalUser = this.userRepository.findById(id);

        //TODO - Throw exception if not found,
        //current implementation is not final!
        return optionalUser.orElse(null);
    }

    /**
     * Find all users
     *
     * @return List with all users in db
     */
    public List<User> list() {
        List<User> users = new ArrayList<>();
        Iterable<User> dbUsers = this.userRepository.findAll();
        dbUsers.forEach(users::add);
        return users;
    }

    /**
     * Update user with specific id
     *
     * @return updated user
     */
    public User update(String id, User user) {
        Optional<User> optionalUser = this.userRepository.findById(id);
        User updatedUser = null;
        if (optionalUser.isPresent()) {
            user.setUser_id(id);
            user.setOn_create(optionalUser.get().getOn_create());

            updatedUser = this.userRepository.save(user);
        }
        return updatedUser;
    }

    /**
     * delete user with specific id
     */
    public void delete(String id) {
        this.userRepository.deleteById(id);
    }

}
