package plainid.training.system.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserHttpController {


    private final UserService userService;

    @Autowired
    public UserHttpController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping
    public User create(@RequestBody User user) {
        return this.userService.create(user);
    }


    @GetMapping("/{id}")
    public User findById(@PathVariable String id) {
        return this.userService.findById(id);
    }


    @GetMapping
    public List<User> list() {
        return this.userService.list();
    }


    @PutMapping("/{id}")
    public User update(@PathVariable String id, @RequestBody User user) {
        return this.userService.update(id, user);
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        this.userService.delete(id);
    }


}
