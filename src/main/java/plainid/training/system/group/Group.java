package plainid.training.system.group;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "groups")
public class Group {

    @Id
    private String group_id;
    private String group_name;

    public Group(String group_id, String group_name) {
        this.group_id = group_id;
        this.group_name = group_name;
    }

    public Group() {

    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }
}
